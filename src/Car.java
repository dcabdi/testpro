public class Car {

    int numberDoor = 4; // Instance Variable
    int numberTear = 4;
    static String engine = "Enginex4"; // static variable
    String brand = "Mercedes";
    String model;

    public Car() {

    }

    public Car(int numDoors, String modelName){
        numberDoor = numDoors;
        model = modelName;
    }

    public Car(int numDoors, String modelName, String brandName){
        numberDoor = numDoors;
        model = modelName;
        brand = brandName;
    }


    public void park() {                                    // void means it doesn't return anything
        System.out.println("Car is parking");
    }

    public void drive() {
        System.out.println("Car is driving");
    }

    public int changeNumberDoors(){
        return 5;
    }
}