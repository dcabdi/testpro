public class Main {

    public static void main(String[]args) {

        Car car1 = new Car(); //created our first object
        System.out.println(car1.brand);
//        System.out.println(car1.numberDoor); // number of doors 4

        car1.numberDoor = 3; // changed the value of doors to 2
//        System.out.println("Number of doors are " + car1.numberDoor);

        car1.model = "C-Class";
        car1.drive();
        car1.numberDoor = 2;
        System.out.println(car1.model);
        System.out.println("Number of doors " + car1.numberDoor);

        System.out.println(Car.engine);
        Car.engine = "Enginex2";
        car1.drive();
        System.out.println("------------------");


        Car car2 = new Car(2, "i5"); // created second object
        car2.numberDoor = 4;
        System.out.println(car2.model);
        System.out.println("Number of doors " + car2.numberDoor);
        car2.model = "C-Class";
        car2.drive();
        System.out.println(car1.changeNumberDoors());
        Car.engine = "Enginex3";
        System.out.println(car2.engine);
        System.out.println("-----------------");

        Car car3 = new Car(3 , "i3", "BMW"); // created second object
        car3.numberDoor = 5;
        System.out.println(car3.model);
        System.out.println("Number of doors " + car3.numberDoor);
        car3.model = "AMG GLE";
        car3.drive();
        System.out.println(car3.changeNumberDoors());
        Car.engine = "Enginex4";
        System.out.println(car3.engine);
        System.out.println("-----------------");








    }
}
